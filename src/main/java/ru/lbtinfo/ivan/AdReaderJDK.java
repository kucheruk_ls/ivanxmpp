package ru.lbtinfo.ivan;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.directory.*;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Класс вычитывает данные из Active Directory
 */
public class AdReaderJDK {
        private ConfigReader configReader;
        private LogWriter logWriter;

        public AdReaderJDK(ConfigReader configReader){
            this.configReader = configReader;
            this.logWriter = new LogWriter(AdReaderJDK.class);
        }

        static DirContext ldapContext;

    /**
     * Вычитывает AD выбирает все объекты, используя их данные создает объекты класса AdUser
     * @return Список объектов класса AdUser содержащий пользователей AD
     */
        public ArrayList<AdUser> ReadAD () {
            ArrayList<AdUser> userArrayList = null;
            try {

                logWriter.print("Начинаем вычитывать AD");

                Hashtable<String, String> ldapEnv = new Hashtable<String, String>();
                ldapEnv.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
                //ldapEnv.put(Context.PROVIDER_URL,  "ldap://10.13.204.1:389");
                String ipad = "ldap://" + configReader.getIPAD();
                ldapEnv.put(Context.PROVIDER_URL, ipad);
                ldapEnv.put(Context.SECURITY_AUTHENTICATION, "simple");
                ldapEnv.put(Context.SECURITY_PRINCIPAL, configReader.getADuser());
                ldapEnv.put(Context.SECURITY_CREDENTIALS, configReader.getADpass());
                ldapContext = new InitialDirContext(ldapEnv);

                // Create the search controls
                SearchControls searchCtls = new SearchControls();

                //Specify the attributes to return
                String returnedAtts[] = {"sn", "givenName", "samAccountName", "mail", "userPrincipalName"};
                searchCtls.setReturningAttributes(returnedAtts);

                //Specify the search scope
                searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

                //specify the LDAP search filter
                String searchFilter = "(&(objectClass=user))";

                //Specify the Base for the search
                String searchBase = configReader.getADOU();
                //initialize counter to total the results
                int totalResults = 0;

                // Search for objects using the filter
                NamingEnumeration<SearchResult> answer = ldapContext.search(searchBase, searchFilter, searchCtls);

                userArrayList = new ArrayList<>();
                //Loop through the search results
                while (answer.hasMoreElements()) {
                    SearchResult sr = (SearchResult) answer.next();

                    totalResults++;


                    //String sn, String givenName, String samAccountName, String userPrincipalName, String mail
                    //System.out.println(">>>" + sr.getName());
                    Attributes attrs = sr.getAttributes();
                    //System.out.println("атрибуты: "+ attrs.toString());


//
                    Attribute sn = attrs.get("sn");
                    String sns = sn!=null?sn.toString():"not set";
                    sns = !sns.isEmpty()?sns.substring(sns.indexOf(": ")+2):"not set";
                    Attribute givenName = attrs.get("givenName");
                    String sgn = givenName!=null?givenName.toString():"";
                    sgn = !sgn.isEmpty()?sgn.substring(sgn.indexOf(": ")+2):"not set";
                    Attribute samAccountName = attrs.get("samAccountName");
                    String san = samAccountName!=null?samAccountName.toString():"";
                    san = !san.isEmpty()?san.substring(san.indexOf(": ")+2):"not set";
                    Attribute userPrincipalName = attrs.get("userPrincipalName");
                    String upn = userPrincipalName!=null?userPrincipalName.toString():"";
                    upn = !upn.isEmpty()?upn.substring(upn.indexOf(": ")+2):"not set";
                    Attribute mail = attrs.get("mail");
                    String smail = mail!=null?mail.toString():"";
                    smail = !smail.isEmpty()?smail.substring(smail.indexOf(": ")+2):"not set";
                    AdUser adUser = new AdUser(sns,sgn,san,upn,smail);
                    userArrayList.add(adUser);
                    //System.out.println("атрибуты: "+ attrs.toString());
                    //System.out.println(">>>>>>" + attrs.get("samAccountName"));
                    //System.out.println(">>>>"+attrs.get("userPrincipalName"));
                    //System.out.println(">>>>>>>> email:" + attrs.get("mail"));
                }
                logWriter.print("Обход AD завершен, прочитано " + totalResults + " записей");

                ldapContext.close();
            } catch (Exception e) {
                logWriter.print("Ошибка чтения из AD " + e);
                logWriter.printStackElements(e.getStackTrace());
                System.exit(-1);
            }
            return userArrayList;
        }
}
