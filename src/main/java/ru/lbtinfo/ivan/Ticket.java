package ru.lbtinfo.ivan;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.io.File;

/**
 * Класс заявка, готов парситься через JSON
 */


@JsonAutoDetect
public class Ticket {
    private boolean alert;
    private boolean autorespond;
    private String source;
    private String name;
    private String email;
    private String phone;
    private String subject;
    private String ip;
    private String message;
    //private File[] attachments;

    public Ticket(String email, String message, String subject) {
        this.alert = true;
        this.autorespond = true;
        this.source = "API";
        this.name = "";
        this.phone = "";
        this.subject = subject;
        this.ip = "";
        this.email = email;
        this.message = message;
        //this.attachments = null;
    }

    public void setAlert(boolean alert) {
        this.alert = alert;
    }

    public void setAutorespond(boolean autorespond) {
        this.autorespond = autorespond;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setMessage(String message) {
        this.message = message;
    }

//    public void setAttachments(File[] attachments) {
//        this.attachments = attachments;
//    }

    public boolean isAlert() {
        return alert;
    }

    public boolean isAutorespond() {
        return autorespond;
    }

    public String getSource() {
        return source;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getSubject() {
        return subject;
    }

    public String getIp() {
        return ip;
    }

    public String getMessage() {
        return message;
    }

//    public File[] getAttachments() {
//        return attachments;
//    }
}
