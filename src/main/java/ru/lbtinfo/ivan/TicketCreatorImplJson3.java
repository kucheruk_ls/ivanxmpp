package ru.lbtinfo.ivan;


import org.jivesoftware.smack.packet.Message;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * задача данного класса вернуть заявку
 *
 *
 */

public class TicketCreatorImplJson3 implements TicketCreator {
    private Message message;
    private LogWriter logWriter = new LogWriter(TicketCreatorImplJson3.class);
    private ConfigReader configReader;
    private ArrayList<AdUser> adUserArrayList;
    String email = "";

    public TicketCreatorImplJson3(Message message, ConfigReader configReader, ArrayList<AdUser> adUserArrayList) {
        //String strtmp = message.getBody().toLowerCase();
       // message.setBody(strtmp);
        this.message = message;
        this.configReader = configReader;
        this.adUserArrayList = adUserArrayList;
    }


    /**
     * Метод создает json представление заявки и передает его в API OSTICKET
     * @return String возвращает результат создания заявки
     */
   // @Override
    public Ticket CreateTicket(){

        String ReturnedValue = "";
        String messageBody = message.getBody();
        String JID = message.getFrom();
        AdUser adUser = null;

        String userPrincipalName = JID.substring(0, JID.indexOf("/")).toLowerCase();

        for(AdUser adUserTmp:adUserArrayList){

            String getUPN = adUserTmp.getUserPrincipalName().toLowerCase();
            String getAccountFull = adUserTmp.getSamAccountName().concat("@").concat(configReader.getBotDomain()).toLowerCase();
            if(getAccountFull.equals(userPrincipalName)){
                String tmpMail = adUserTmp.getMail();
                adUser = adUserTmp;
                if(tmpMail.equals("not set"))
                    tmpMail=getAccountFull;

                email=tmpMail;
            }
        }
        //SubjectCreator subjectCreator = new SubjectCreator(message,configReader);
        //String subject = subjectCreator.Create();
        String subject = message.getBody();
//        String name = " "+adUser.getSn()+" "+adUser.getGivenName();
        if(subject==""||subject==null){
            subject = messageBody.length()<49?messageBody:messageBody.substring(0,49);
        }
//        subject = subject+name;
        Ticket ticket = new Ticket(email, messageBody, subject);

        return ticket;
    }

    /**
     * Метод создает заявку админу указанному в конфиг файле в случае неудачи создания заявки методом
     * CreateTicket();
     * @return
     */
    public Ticket CreateAdminTicket() {

        String ReturnedValue = "";
        String messageBody = message.getBody();
        String JID = message.getFrom();
        String email = configReader.getOsticketAdmin();
        messageBody = "Ошибка создания заявки через бота: Пользователь: JID: "+ JID+" Текст заявки: "+messageBody;

        Ticket ticket = new Ticket(email, messageBody, "Ошибка создания заявки ботом...");
        //писать результат сериализации будем во врайтер

        return ticket;
    }

    /**
     * данный метод при обнаружении в теле ticket емайла указанного в конфигурационном файле заменяет его
     * на указанный для замены и возвращает заявку
     * @param ticket
     * @return
     */
    public Ticket emailReplacer(Ticket ticket){

        logWriter.print("Проверим есть ли в заявке адреса из списка замены");
        logWriter.print("распечатаем весь список замен");
        HashMap<String,String> map = configReader.getEmailReplace();
        for(Map.Entry<String,String> entry:map.entrySet()){
            logWriter.print(entry.getKey()+"-"+entry.getValue());
        }
        if(map.containsKey(ticket.getEmail())){
            ticket.setEmail(map.get(ticket.getEmail()));
        }
        return ticket;
    }




}
