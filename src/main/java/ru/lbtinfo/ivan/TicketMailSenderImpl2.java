package ru.lbtinfo.ivan;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.Properties;

public class TicketMailSenderImpl2 implements TicketSender {
    private ConfigReader configReader;
    private LogWriter logWriter;

    public TicketMailSenderImpl2(ConfigReader configReader){
        this.configReader = configReader;
        this.logWriter = new LogWriter(TicketMailSenderImpl2.class);
    }

    /**
     *отправляю заявку на емаил
     *
     *
     */
    @Override
    public String sendTicket(Ticket ticket){
        if(ticket!=null) {
//String to1 = configReader.getReceiverMail(); //адресат
//String to2 = ConfigReader.getReceiverMail2(); //адресат 2
            String zayavkaMailAddress = configReader.getReceiverMail();


            String from = ticket.getEmail();//отправитель
            String host = configReader.getSmtpServer();//smtp сервер
            String port = configReader.getSmtpPort();//порт smtp сервера
            String enablessl = configReader.getEnableSsl();//используется ли ssl текстовое true/false
            String enableauth = configReader.getEnableAuth();//сервер использует автризацию?
            final String loginmail = configReader.getLoginSmtp();//логин к почте
            final String passmail = configReader.getPassSmtp();//пароль к почте
//            String providerlogin = configReader.getProviderLogin();//логин провайдера для рассылки
//            String providerpass = configReader.getProviderPsw();//пароль провайдера для рассылки
//            String providersender = configReader.getProviderSender();//имя отправителя для sms

            Properties propertiesM = System.getProperties();
            propertiesM.setProperty("mail.smtp.host", host);
            propertiesM.setProperty("mail.smtp.port", port);
            propertiesM.setProperty("mail.smtp.ssl.enable", enablessl);
            propertiesM.setProperty("mail.smtp.auth", enableauth);


            Session session = Session.getDefaultInstance(propertiesM, new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(loginmail, passmail);
                }
            });
            try {
                MimeMessage message = new MimeMessage(session);//сообщение электронной почты
                message.setFrom(new InternetAddress(from)); //настройка полей заголовка
                message.addRecipients(Message.RecipientType.TO, zayavkaMailAddress);
                message.setSubject(ticket.getSubject() + ""); //тема сообщения

                Multipart multipart = new MimeMultipart();
                MimeBodyPart textBodyPart = new MimeBodyPart();


                textBodyPart.setText(ticket.getMessage(), "UTF-8");
                multipart.addBodyPart(textBodyPart);

                message.setContent(multipart);
                logWriter.print("заявка "+ticket.getSubject()+" готова к отправке в систему посредством электронной почты");

                //отправка сообщения

                Transport.send(message);
                logWriter.print(this.getClass().getSimpleName() + ": " + "Cообщение отправлено...");
                return "Created";

            } catch (MessagingException e) {
                logWriter.print(this.getClass().getSimpleName() + ": " + "ошибка в теле письма");
                logWriter.printStackElements(e.getStackTrace());
                return "ошибка в теле письма!";

            }
        }else{
            logWriter.print(this.getClass().getSimpleName()+": "+"нечего отправлять!");
            return "нечего отправялть";
        }
    }
}
