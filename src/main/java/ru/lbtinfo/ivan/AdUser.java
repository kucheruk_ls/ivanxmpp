package ru.lbtinfo.ivan;

/**
 * объекты класса создаются с использованием данных полученых из AD классом AdReaderJDK
 */
public class AdUser {
    //"sn","givenName", "samAccountName", "mail", "userPrincipalName"
    private String sn; // Фамилия Имя Отчество
    private String givenName; //Имя
    private String samAccountName;//Логин домена
    private String userPrincipalName; //полное доменное имя пользователя
    private String mail;//электронная почта

    public AdUser(String sn, String givenName, String samAccountName, String userPrincipalName, String mail) {
        this.sn = sn;
        this.givenName = givenName;
        this.samAccountName = samAccountName;
        this.userPrincipalName = userPrincipalName;
        this.mail = mail;
    }

    public String getSn() {
        return sn;
    }

    public String getGivenName() {
        return givenName;
    }

    public String getSamAccountName() {
        return samAccountName;
    }

    public String getUserPrincipalName() {
        return userPrincipalName;
    }

    public String getMail() {
        return mail;
    }

    @Override
    public String toString() {
        return "AdUser{" +
                "sn='" + sn + '\'' +
                ", givenName='" + givenName + '\'' +
                ", samAccountName='" + samAccountName + '\'' +
                ", userPrincipalName='" + userPrincipalName + '\'' +
                ", mail='" + mail + '\'' +
                '}';
    }
}
