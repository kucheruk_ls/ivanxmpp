package ru.lbtinfo.ivan;

import org.jivesoftware.smack.packet.Message;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Задача класса - получить тему сообщения из текста.
 * тема будет в формате ключевое слово(из перечня ключевых слов в конфигурационном файле - ключевые слова Паттерны regexp): пользователь
 */
public class SubjectCreator {
    private Message message;
    private ConfigReader configReader;
    private LogWriter logWriter;

    public SubjectCreator(Message message, ConfigReader configReader) {
        this.message = message;
        this.configReader = configReader;
        this.logWriter = new LogWriter(SubjectCreator.class);
    }

    public String Create(){
        String messageBody = message.getBody().toLowerCase();

        String messageSub = message.getSubject();
        if(!messageBody.matches("^%.*")){
        HashMap<String,String> map = new HashMap<>();
        String config = configReader.getSubjectKeyWord();
        String[] tmpm = config.split(";");
        logWriter.print("Прочитал файл конфигурации, начинаю анализ");
        logWriter.print("Выведу пары значений:");

            for (String s : tmpm) {

                String[] ms = s.split("-");
                logWriter.print(ms[0] + ":" + ms[1]);
                map.put(ms[0], ms[1]);
            }

        for(Map.Entry<String,String> entry:map.entrySet()) {
            System.out.println(entry.getKey() + " в случае обнаружения заменим на " + entry.getValue());
            Pattern pattern = Pattern.compile(entry.getKey().toLowerCase());
            Matcher matcher = pattern.matcher(messageBody);

            if (matcher.find()) {
                logWriter.print("Нашел совпадение в тексте. Добавляю в тему " + entry.getValue());
                messageSub = entry.getValue();
            }
        }
            System.out.println("Тема: "+messageSub);
        }else{
            //если мы здесь значит тело сообщения начинается на % сначала удалим процент и командное слово за ним
            String tmp = messageBody.toLowerCase().substring(messageBody.indexOf(" ")+1);
            message.setBody(tmp);
            //tmp =
            //затем сформируем тему
            messageSub = tmp.length()<50?tmp:tmp.substring(0,50);
        }




        return messageSub;
    }
}
