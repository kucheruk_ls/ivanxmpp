package ru.lbtinfo.ivan;

import java.io.File;
import java.io.IOException;

/**
 * xmpp bot!
 */

public class App {
    static ConfigReader configReader;

    public static void main(String[] args) {
        //ConfigReader configReader = null;
        try {
            configReader = new ConfigReader(new File("config.xml"));

        } catch (IOException e) {
            e.printStackTrace();
        }

        String botNick = configReader.getBotNick();
        String botPassword = configReader.getBotPassword();
        String botDomain = configReader.getBotDomain();
        String botServer = configReader.getBotServer();
        int botPort = configReader.getBotPort();


        try {


            JabberBot bot = new JabberBot(botNick, botPassword, botDomain, botServer, botPort, configReader);
            Thread botThread = new Thread(bot);
            botThread.start();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}

