package ru.lbtinfo.ivan;


/**
 * интерфейс получения данных о состоянии включения света в помещениях
 */
public interface LookingForBathroom {

    public boolean ManBathRoomControl();

    public boolean WomanBathRoomControl();
}
