package ru.lbtinfo.ivan;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Я пишу в консоль и лог! Файл IvanXmpp.log находится в рабочей директории программ(там где *.jar). Если его там нет, он будет создан!
 *
 * @author leonid s kucheruk  shtormlbt@mail.ru
 * @version 1.1.0
 * в конструктор передается имя класса и используется при выводе
 * методы перестали быть статическими
 * version 1.0.0
 */
public class LogWriter {
    private Class clazz;
    private ConfigReader configReader = App.configReader;

    public LogWriter(Class clazz) {
        this.clazz = clazz;
    }

    /**
     * метод пишет в консоль и лог строку полученную в качестве параметра - добавляя в начале строки дату получения строки и имя класса передавшего строку
     * в виде 2018:12:16 11:13:22 | имя класса | параметр string
     *
     * @param string - сообщение которое будет записано в лог и консоль
     */
    public synchronized void print(String string) {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY:MM:dd HH:mm:ss");
        String dt = sdf.format(date);

        Path logfile = Paths.get("IvanXmpp.log");
        if (Files.notExists(logfile)) {
            try {
                Files.createFile(logfile);
            } catch (IOException e) {
                System.out.println("En : error creating log file");
                System.out.println("Ru : Ошибка создания лог файла");
            }
        }
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(logfile.toFile(), true);
            try (BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
                bufferedWriter.append(dt + " | " + clazz.getName() + " | " + string + System.lineSeparator());

            }
        } catch (IOException e) {
            System.out.println("En : I cannot write to the log (((at all. Something is wrong with it!");
            System.out.println("Ru : В лог не могу писать((( совсем. Что то с ним не так!");
            e.printStackTrace();
        }

        System.out.println(dt + " | " + clazz.getName() + " | " + string);

    }

    /**
     * метод получает в качестве параметра массив элементов стека и построчно выводит их в лог и консоль
     *
     * @param elements - стэк
     */
    public synchronized void printStackElements(StackTraceElement[] elements) {
        if (configReader.getLogLevel() > 1) {
            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("YYYY:MM:dd HH:mm:ss");
            String dt = sdf.format(date);

            Path logfile = Paths.get("IvanXmpp.log");
            if (Files.notExists(logfile)) {
                try {
                    Files.createFile(logfile);
                } catch (IOException e) {
                    System.out.println("En : error creating log file");
                    System.out.println("Ru : Ошибка создания лог файла");
                }
            }
            FileWriter fileWriter = null;
            try {
                fileWriter = new FileWriter(logfile.toFile(), true);
                try (BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {

                    for (StackTraceElement el : elements) {
                        bufferedWriter.append(dt + " | " + clazz.getName() + " | " + el.toString() + System.lineSeparator());
                    }

                }
            } catch (IOException e) {
                System.out.println("En : I cannot write to the log (((at all. Something is wrong with it!");
                System.out.println("Ru : В лог не могу писать((( совсем. Что то с ним не так!");
                e.printStackTrace();
            }

            for (StackTraceElement el : elements) {
                System.out.println(dt + " | " + clazz.getName() + " | " + el.toString());
            }

        }
    }

}
