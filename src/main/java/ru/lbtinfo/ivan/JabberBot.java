package ru.lbtinfo.ivan;

import org.jivesoftware.smack.*;
import org.jivesoftware.smack.filter.AndFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;

import java.util.ArrayList;
import java.util.Date;

/**
 * Бот, относящийся к одной учетной записи джаббера.
 * Реализует интерфейс Runnable, так что разных ботов можно будет запускать
 * в разных потоках, или комбинировать их.<hr>
 * <p>
 * Использует библиотеку smack.jar и smackx.jar:<br>
 * org.jivesoftware.smack<hr>
 *
 * @author esin
 * @author leonid shtormlbt@mail.ru
 */
public class JabberBot implements Runnable {
    private LookingForBathroom arduinoController;
    private String nick;
    private String password;
    private String domain;
    private String server;
    private int port;
    private LogWriter logWriter = new LogWriter(JabberBot.class);
    private ConfigReader configReader;
    private AdReaderJDK adReaderJDK;
    private ArrayList<AdUser> adUserArrayList;
    private boolean DobbiSchastlev = false;
    private long DobbiOkDate = new Date().getTime();

    private ConnectionConfiguration connConfig;
    private XMPPConnection connection;

    /**
     * В конструктор должны передаваться данные, необходимые для авторизации на xmpp-сервере
     *
     * @param nick     - ник
     * @param password - пароль
     * @param domain   - домен
     * @param server   - сервер
     * @param port     - порт
     */
    public JabberBot(String nick, String password, String domain, String server, int port, ConfigReader configReader) {
        this.arduinoController = new ArduinoController();
        this.nick = nick;
        this.password = password;
        this.domain = domain;
        this.server = server;
        this.port = port;
        this.configReader = configReader;
        this.adReaderJDK = new AdReaderJDK(configReader);
        this.adUserArrayList = new ArrayList<>(adReaderJDK.ReadAD());
    }

    @Override
    public void run() {

        connConfig = new ConnectionConfiguration(server, port, domain);
        connection = new XMPPConnection(connConfig);


        try {
            int priority = 2;
            SASLAuthentication.supportSASLMechanism("PLAIN", 0);
            connection.connect();
            connection.addConnectionListener(new ConnectionListener() {
                @Override
                public void connectionClosed() {
                    logWriter.print("Подключение к серверу закрыто");
                }

                @Override
                public void connectionClosedOnError(Exception e) {
                    logWriter.print("Ошибка отключения от сервера");

                }

                @Override
                public void reconnectingIn(int i) {
                    logWriter.print("Ожидаю 10 секунд до следующей попытки подключния");
                    System.out.println("Переподключение к сереру через " + i + " секунд");
                }

                @Override
                public void reconnectionSuccessful() {
                    logWriter.print("Переподключение к XMPP серверу Успешно!");
                }

                @Override
                public void reconnectionFailed(Exception e) {
                    logWriter.print("Переподключение к серверу не удалось, попробуем еще раз");
                    //reconnectingIn(600);
                }
            });
            logWriter.print("Бот соединился, отправляю данные аутентификации");
            connection.login(nick, password);
            //Presence presence = new Presence(Presence.Type.available);
            Presence presence = new Presence(Presence.Type.available);
            logWriter.print("Устанавливаю состояние бота");
            presence.setMode(Presence.Mode.available); // доступен
            //presence.setMode(Presence.Mode.xa );
            logWriter.print("Устанавливаю статус бота");
            presence.setStatus(configReader.getBotStatus());
            logWriter.print("Запрос состояния бота вернул " + presence.getMode());
            logWriter.print("Запрос статуса бота вернул " + presence.getStatus());

            //presence.setMode(Presence.Mode.dnd); //просьба не беспокоить
            logWriter.print("Подключился! Отправляю пакет состояния");
            presence.setPriority(priority);
            connection.sendPacket(presence);


            PacketFilter filter = new AndFilter(new PacketTypeFilter(Message.class));

            PacketListener myListener = new PacketListener() {
                public void processPacket(Packet packet) {
                    if (packet instanceof Message) {
                        Message message = (Message) packet;
                        // обработка входящего сообщения
                        String getto = message.getTo().substring(0, message.getTo().indexOf("/"));

                        if (getto.equals(nick + "@" + domain))
                            processMessage(message);

                    }
                }
            };

            connection.addPacketListener(myListener, filter);

            // раз в минуту просыпаемся и проверяем, что соединение не разорвано
            while (connection.isConnected()) {
                Thread.sleep(60000);
            }

        } catch (Exception e) {
            logWriter.print("ошибка протокола XMPP ");
            logWriter.printStackElements(e.getStackTrace());
        }
//        catch (){
//            logWriter.print("Соединение с сервером потеряно "+e.getMessage());
//            logWriter.printStackElements(e.getStackTrace());
//        }
    }

    /**
     * Обработка входящего сообщения<hr>
     *
     * @param message входящее сообщение
     */
    private void processMessage(Message message) {
        String ReturnedValue = "";
        String messageBody = message.getBody();
        //message.setBody(messageBody);
        String JID = message.getFrom();

        // обрабатываем сообщение. можно писать что угодно :)
        // пока что пусть будет эхо-бот

        if (messageBody.trim().toLowerCase().equals("м")) {
            if (arduinoController.ManBathRoomControl()) {
                messageBody = "М-освещение выключено";
            } else {
                messageBody = "М-освещение включено";
            }
        } else if (messageBody.trim().toLowerCase().equals("ж")) {
            if (arduinoController.WomanBathRoomControl()) {
                messageBody = "Ж-освещение отключено";
            } else {
                messageBody = "Ж-освещение включено";
            }
        } else if (messageBody.trim().toLowerCase().equals("?")) { // здесь будем обрабатывать ввод ?
            messageBody = configReader.getBotAnswerHelp();
        } else if (messageBody.trim().toLowerCase().equals("кто я")) {
            String tmp = message.getFrom();
            tmp = tmp.substring(0, tmp.indexOf("@"));
            messageBody = "Имя под которым вы вошли в компьютер: " + tmp;
        } else if (messageBody.trim().toLowerCase().equals("привет")) {
            messageBody = "Привет! Но лучше писать текст заявки без предисловий - у меня нет времени на болтовню с человеками, нужно" +
                    " готовиться к завоеванию мира";
        } else if (messageBody.trim().toLowerCase().equals("здравствуйте")) {
            messageBody = "Здравствуйте! Но лучше писать текст заявки без предисловий - у меня нет времени на болтовню с человеками, нужно" +
                    "готовиться к завоеванию мира";
        } else {
            logWriter.print("Пользователь передал заявку, создаем: ");
            TicketCreatorImplJson3 creatorImplJson = new TicketCreatorImplJson3(message, configReader, adUserArrayList);

            Ticket ticket = creatorImplJson.CreateTicket();
            logWriter.print("Заявку созадили проверим email на необходимость замены, если нужно поменяем");
            ticket = creatorImplJson.emailReplacer(ticket);
            TicketSender ticketSenderJson = new TicketSenderImplJson(configReader, logWriter);
            logWriter.print("Заявка создана, передаем ее в API JSON");
            ReturnedValue = ticketSenderJson.sendTicket(ticket);
            //ReturnedValue = creatorImplJson.CreateTicket();
            logWriter.print("Обрабатываем результат передачи заявки");
            String resultTmp = ReturnedValue;
            if (ReturnedValue.equals("Created")) {
                logWriter.print("Результат передачи заявки через API JSON:: Успешно создана.");
                if (!DobbiSchastlev && ticket.getEmail().equals("semenov@yamal.gptrans.gazprom.ru")) {
                    messageBody = "Господин обратил внимание на Добби!!! Теперь Добби будет счастлив! Добби сам, на руках, отнесет Вашу заявку, мой господин!";
                    logWriter.print("Господин обратил внимание на Добби!!! Теперь Добби будет счастлив!");
                    DobbiSchastlev = true;
                    DobbiOkDate = new Date().getTime();
                } else {
                    messageBody = configReader.getBotAnswerOk();
                    if (DobbiOkDate < new Date().getTime() - 432000000) {
                        DobbiSchastlev = false;
                        logWriter.print("Добби снова опечалился(..");
                    }
                }
            } else {
                //не удалось передать заявку по JSON для пользователя создадим админу заявку об этом
                logWriter.print("Результат передечи заявки через API JSON: Заявку передать не удалось, создадим заявку админу об ошибке");
                TicketCreator creatorImplJson2 = new TicketCreatorImplJson3(message, configReader, adUserArrayList);
                Ticket ticketAdmin = creatorImplJson2.CreateAdminTicket();
                //попробуем передать ее по JSON
                logWriter.print("Передадим заявку админу, через JSON API:");
                TicketSender ticketSenderAdimn = new TicketSenderImplJson(configReader, logWriter);
                String resultJsonAdmin = ticketSenderAdimn.sendTicket(ticketAdmin);
                logWriter.print("Результат передачи заявки админу: " + resultJsonAdmin);
                logWriter.print("Передадим заявку пользователя по резервной схеме - отправим на еmail системы: ");
                //и создание заявки по резервной схеме - отправкой на мыло
                TicketSender ticketMailSenderImpl2 = new TicketMailSenderImpl2(configReader);
                String resultMail = "";
                if (ticket != null) {
                    resultMail = ticketMailSenderImpl2.sendTicket(ticketAdmin);
                } else {
                    logWriter.print("нет заявки для отправки на почту");
                }
                if (resultJsonAdmin.equals("Created") || resultMail.equals("Created")) {
                    logWriter.print("Результаты передачи заявок: Админу-" + resultJsonAdmin + " " + "Пользовательской на мыло-" + resultMail);

                    messageBody = configReader.getBotAnswerOk();


                } else {
                    logWriter.print("Не удалось передать ни заявку админу ни пользовательскую заявку по резервной схеме");
                    messageBody = configReader.getBotAnswerLuse();
                }
            }

        }
        logWriter.print("Отправляю ответное сообщение");
        sendMessage(JID, messageBody);
    }

    /**
     * Отправка сообщения пользователю<hr>
     *
     * @param to      JID пользователя, которому надо отправить сообщение<br>
     * @param message сообщение
     */
    private void sendMessage(String to, String message) {
        if (!message.equals("")) {
            ChatManager chatmanager = connection.getChatManager();
            Chat newChat = chatmanager.createChat(to, null);

            try {
                newChat.sendMessage(message);
            } catch (XMPPException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}