package ru.lbtinfo.ivan;

public interface TicketSender {


    /**
     * метод возвращает результат отправки в String т.к. возможно возвращение значений обработки пост запросов
     * в случае возвращения методом булевого значения true = Created false = любое другое
     * @param ticket
     * @return
     */
    public String sendTicket(Ticket ticket);
}
