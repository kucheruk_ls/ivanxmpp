package ru.lbtinfo.ivan;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class TicketSenderImplJson implements TicketSender {
    private ConfigReader configReader;
    private LogWriter logWriter;

    public TicketSenderImplJson(ConfigReader configReader, LogWriter logWriter) {
        this.configReader = configReader;
        this.logWriter = logWriter;
    }

    @Override
    public String sendTicket(Ticket ticket) {
        String ReturnedValue = "";
        StringWriter writer = new StringWriter();
        //это объект Jackson, который выполняет сериализацию
        ObjectMapper mapper = new ObjectMapper();

        try {
            mapper.writeValue(writer, ticket);
        } catch (
                JsonMappingException e) {
            logWriter.print("Ошибка маппинга json");
            logWriter.printStackElements(e.getStackTrace());
        } catch (
                JsonGenerationException e) {
            logWriter.print("Ошибка генерирования json");
            logWriter.printStackElements(e.getStackTrace());
        } catch (
                IOException e) {
            logWriter.print("Ошибка ввода/вывода");
            logWriter.printStackElements(e.getStackTrace());
        }

        //тестовый вывод
        System.out.println(writer.toString());



        //будем пулять json дальше в сеть
        //String url="https://requestbin.jumio.com/13cg7zs1";
        String url = configReader.getAPIURL();
        URL object = null;
        try{
            object=new URL(url);
            HttpURLConnection con = (HttpURLConnection) object.openConnection();
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Accept", "application/json");
            con.setRequestProperty("X-API-Key",configReader.getAPIKEY());
            con.setRequestMethod("POST");
            OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream(),"utf-8");
            wr.write(writer.toString());
            wr.flush();

            //display what returns the POST request

            StringBuilder sb = new StringBuilder();
            int HttpResult = con.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(
                        new InputStreamReader(con.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
                //System.out.println("" + sb.toString());
                logWriter.print(""+sb.toString());
            } else {

                logWriter.print("" + con.getResponseMessage());


                ReturnedValue = con.getResponseMessage();



            }
        }catch (MalformedURLException e){
            logWriter.print("МалформедУРЛЭксепшен");
            logWriter.printStackElements(e.getStackTrace());
        }catch (IOException e){
            logWriter.print("Ошибка ввода/вывода");
            logWriter.printStackElements(e.getStackTrace());
        }
        return ReturnedValue;




        //return "Create";
    }
}
