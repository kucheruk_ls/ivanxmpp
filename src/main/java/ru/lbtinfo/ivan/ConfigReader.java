package ru.lbtinfo.ivan;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;


/**
 * класс читает файл конфигурации
 */
public class ConfigReader {
    private File file;
    private Properties properties;

    public ConfigReader(File file) throws IOException {
        properties = new Properties();
        properties.loadFromXML(new FileInputStream(file));
    }

    public String getBotNick() {
        return properties.getProperty("botNick");
    }

    public String getBotPassword() {
        return properties.getProperty("botPassword");
    }

    public String getBotDomain() {
        return properties.getProperty("botDomain");
    }

    public String getBotServer() {
        return properties.getProperty("botServer");
    }

    public int getBotPort() {
        return Integer.parseInt(properties.getProperty("botPort"));
    }

    public String getAPIURL() {
        return properties.getProperty("APIURL");
    }

    public String getAPIKEY() {
        return properties.getProperty("APIKEY");
    }

    public String getIPAD() {
        return properties.getProperty("IPAD");
    }

    public String getADuser() {
        return properties.getProperty("ADuser");
    }

    public String getADpass() {
        return properties.getProperty("ADpass");
    }

    public String getADOU() {
        return properties.getProperty("ADOU");
    }

    public String getOsticketAdmin() {
        return properties.getProperty("osticketAdmin");
    }

    public String getBotAnswerOk() {
        return properties.getProperty("BotAnswerOK");
    }

    public String getBotAnswerLuseAdminOk() {
        return properties.getProperty("BotAnswerLuseAdminOk");
    }

    public String getBotAnswerLuse() {
        return properties.getProperty("BotAnswerLuse");
    }

    public String getBotStatus() {
        return properties.getProperty("BotStatus");
    }

    public String getBotAnswerHelp() {
        return properties.getProperty("BotAnswerHelp");
    }

    public int getLogLevel() {
        int i = Integer.parseInt(properties.getProperty("LogLevel"));
        return i;
    }

    public String getSubjectKeyWord() {
        return properties.getProperty("SubjectKeyWord");
    }

    public String getReceiverMail() {
        return properties.getProperty("receiverMail");
    }

    public String getSmtpServer() {
        return properties.getProperty("smtpServer");
    }

    public String getSmtpPort() {
        return properties.getProperty("smtpPort");
    }

    public String getEnableSsl() {
        return properties.getProperty("enableSsl");
    }

    public String getEnableAuth() {
        return properties.getProperty("enableAuth");
    }

    public String getLoginSmtp() {
        return properties.getProperty("loginSmtp");
    }

    public String getPassSmtp() {
        return properties.getProperty("passSmtp");
    }

    public HashMap<String, String> getEmailReplace() {
        String tmp = properties.getProperty("emailReplace");
        String[] tmpm = tmp.split(";");

        HashMap<String, String> map = new HashMap<>();
        for (String s : tmpm) {
            String[] t = s.split("-");
            map.put(t[0], t[1]);
        }
        return map;
    }
}
